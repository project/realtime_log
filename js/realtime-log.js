(function ($, Drupal, drupalSettings) {

  Drupal.Nodejs.callbacks.realtimeLog = {
    callback: function (message) {
      Drupal.nodejs_ajax.runCommands(message);
    }
  };

})(jQuery, Drupal, drupalSettings);

