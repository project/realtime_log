<?php

namespace Drupal\realtime_log\Twig;

/**
 * Provides the Debug log debugging function within Twig templates.
 */
class RealtimeLogExtension extends \Twig_Extension {

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'rl';
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return array(
      new \Twig_SimpleFunction('rl', array($this, 'rl'), array(
        'needs_environment' => TRUE,
        'needs_context' => TRUE,
      )),
    );
  }

  /**
   * Provides rl function to Twig templates.
   *
   * @param Twig_Environment $env
   *   The twig environment instance.
   * @param array $context
   *   An array of parameters passed to the template.
   */
  public function rl(\Twig_Environment $env, array $context) {
    // Don't do anything unless twig_debug is enabled. This reads from the Twig
    if (!$env->isDebug()) {
      return;
    }

    if (func_num_args() === 2) {
      // No arguments passed, display full Twig context.
      $rl_variables = array();
      foreach ($context as $key => $value) {
        if (!$value instanceof \Twig_Template) {
          $rl_variables[$key] = $value;
        }
      }
      rl($rl_variables, $this->t('Context as array'));
    }
    else {
      $args = array_slice(func_get_args(), 2);
      if (isset($args[1])) {
        rl($args[0], (string) $args[1]);
      }
      else {
        rl($args[0]);
      }
    }
  }

}
