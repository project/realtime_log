<?php

namespace Drupal\realtime_log\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class RealtimeLogDeleteForm extends FormBase {

  /**
   * @inheritdoc
   */
  public function getFormId() {
    return 'rl_delete_form';
  }

  /**
   * @inheritdoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['rl_clear'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Clear debug log messages'),
      '#description' => $this->t('This will permanently remove the log messages from the database.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['rl_clear']['clear'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Clear log messages'),
    );

    return $form;
  }

  /**
   * @inheritdoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    Database::getConnection()->delete('realtime_log')
      ->execute();
    $this->messenger()->addStatus($this->t('All debug messages have been cleared.'));
  }
}
