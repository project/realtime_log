<?php

namespace Drupal\realtime_log\Commands;

use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 */
class RealtimeLogCommands extends DrushCommands {

  /**
   * Clear realtime logs.
   *
   * @command realtime_log:clearLog
   * @aliases rlcl
   */
  public function clearLog() {
    // TODO: Drupal Rector Notice: Please delete the following comment after you've made any necessary changes.
    // You will need to use `\Drupal\core\Database\Database::getConnection()` if you do not yet have access to the container here.
    \Drupal::database()->delete('realtime_log')->execute();
    $this->logger()->success(dt('Realtime logs cleared.'));
  }

}
