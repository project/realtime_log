Realtime Log
------------

- Install drupal-node.js package with npm somewhere outside of drupal web root:

  npm i uniquename/drupal-nodejs

  Use the forked module until https://github.com/beejeebus/drupal-nodejs/pull/39 has landed.

- Follow the install instructions in the drupal-node.js NPM package.
  https://github.com/uniquename/drupal-nodejs/blob/master/nodejs.config.js.example

- Configure drupal/nodejs module at admin/config/nodejs/settings, mainly set the Service Key
  you used in nodejs.config.js

- Then start a local server with:

  node node_modules/drupal-node.js/app.js

- Go to /admin/reports/rlog to receive debug messages.

- Use rl($message, $title) to send messages from anywhere in code.
